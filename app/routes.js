var mysql = require('mysql');
var dbconfig = require('../config/database');
var connection = mysql.createConnection(dbconfig.connection);

connection.query('USE ' + dbconfig.database);

// app/routes.js
module.exports = function(app, passport) {

	app.get('/', function(req, res) {
		res.render('index.ejs'); // load the index.ejs file
	});

	app.get('/login', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	

	// show the signup form
	app.get('/signup', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/login', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

// call the api for signup form
app.post('/api/signup', function(req, res, next) {
	passport.authenticate('local-signup', function(err, user) {
	
			res.setHeader('Content-Type', 'application/json');
			var message;
			if(user)
				message ="User added successfully";
				else
				message="User is already present";
			res.send(JSON.stringify({
				message:message
			
					
			}));
		
	})(req, res, next);
});
	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	
	// call the api for login form
	app.post('/api/login', function(req, res, next) {
		passport.authenticate('local-login', function(err, user ) {
			req.logIn(user, function() {
				res.setHeader('Content-Type', 'application/json');
				var message;
				if(user){
					message="Log in suessfull";
				}
				else{
					message="Wrong username or password";
					}
				res.send(JSON.stringify({
					message: message
			
				}));
			});
		})(req, res, next);
	});
	//call the api for profile form
	app.post('/api/profile', function(req, res, next) {
		passport.authenticate('local-login', function(err, user) {
			req.logIn(user, function() {
				res.setHeader('Content-Type', 'application/json');
				
				res.send(JSON.stringify({
					
					Profile: {
						username:user.username,
						email:user.email,
						mobile:user.mobile,
						dob:user.dob,
						location:user.location
					}
					
				
				}));
			});
		})(req, res, next);
	});
	

	//rest api to update record into mysql database
	app.put('/api/user', function (req, res,next) {
	   connection.query("UPDATE `users` SET `username`=?,`email`=?,`password`=?,`mobile`=?,`dob`=?,`location`=? where `id`=?", [req.body.username,req.body.email,req.body.password,req.body.mobile,req.body.dob,req.body.location,req.body.id], function (error, results, fields) {
	   if (error){ 
		console.log(error);
	   }
	 
	   res.setHeader('Content-Type', 'application/json');
	   res.send(JSON.stringify(results));

	 });(req, res, next);
	});

//api to remove an account
	app.delete('/api/remove', function (req, res,next) {
	
		connection.query("DELETE FROM `users` WHERE `id`=?", [req.body.id], function (error, results, fields) {
			if (error){ 
				console.log(error);
			   }
			   console.log(results);
			   res.setHeader('Content-Type', 'application/json');
			   res.send(JSON.stringify("record has been deleted"));
	  });
	 });

	
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});


	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}
}